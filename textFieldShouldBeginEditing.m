- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(done:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer: tapGestureRecognizer];   //只需要点击非文字输入区域就会响应hideKeyBoard
    [tapGestureRecognizer release];
     
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.3f];
    float width=self.view.frame.size.width;
    float height=self.view.frame.size.height;
    CGRect rect=CGRectMake(0.0f,-80*(textField.tag),width,height);//上移80个单位，一般也够用了
    self.view.frame=rect;
    [UIView commitAnimations];
     
    return YES;
}
 
-(void)done:(id)sender
{
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            [view resignFirstResponder];
             
            [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
            [UIView setAnimationDuration:0.3f];
            float width=self.view.frame.size.width;
            float height=self.view.frame.size.height;
            CGRect rect=CGRectMake(0.0f,0.0f,width,height);
            self.view.frame=rect;
            [UIView commitAnimations];
        }
    }
     
}